from django.shortcuts import render

def Home(request):
    try:
        template_name = "product/product.html"
        context = {}
        return render(request, template_name, context)
    except Exception as e:
        print("Error in rendering ",e)