CREATE TABLE public.product_product
(
  id integer NOT NULL DEFAULT nextval('product_productdesc_id_seq'::regclass),
  item_code character varying(512),
  item_name character varying(512),
  category_l1 character varying(512),
  category_l2 character varying(512),
  child_code character varying(512),
  parent_code_path character varying(512),
  mrp_price integer,
  size character varying(100),
  enabled boolean NOT NULL,
  date_created date,
  date_updated date,
  CONSTRAINT product_productdesc_pkey PRIMARY KEY (id)
)