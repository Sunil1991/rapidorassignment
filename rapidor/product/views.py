import simplejson
import json
from django.db.models import Avg, Q
from django.http import HttpResponse, JsonResponse
from rest_framework import viewsets, status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from utility.utility import read_excel

from .service import get_parent_path_dict,inset_dataframe_db
from .models import Product
from .serializers import ProductSerializer
from .const import QueryType
"""
Assuming excecl has been uploaded from front end to directory path /rapidor/exccelupload/item.xlsx
"""

class ProductViewset(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    allowed_methods = ('GET','POST','PUT',)
    def create(self, request, *args, **kwargs):
        try:
            try:
                data_frame = read_excel("items.xlsx")
            except Exception as e:
                print("Error in reading excel ",e)
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            child_parent_dict = get_parent_path_dict(data_frame)
            try:
                inset_dataframe_db(data_frame,child_parent_dict)
            except Exception as e:
                print("Error in saving excel records",e)
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            response = Response(status=status.HTTP_201_CREATED)
        except Exception as e:
            print("Error in post Viwset",e)
        return response

    """"
    Handling Queries
    """
    def list(self, request, *args, **kwargs):
        product_queryset = Product.objects.filter()
        if int(request.GET['flag']) == QueryType.LOAD.value:
            serializer = ProductSerializer(
                product_queryset, many=True, context={'request': request})
            json = JSONRenderer().render(serializer.data)
            return HttpResponse(json, content_type="application/json")
        if int(request.GET['flag']) == QueryType.SEARCH_BY_PRODUCT:
            product_name = str(request.GET['product_name'])
            product_queryset = product_queryset.filter(item_name=product_name).order_by('child_code')
            serializer = ProductSerializer(
                product_queryset, many=True, context={'request': request})
            json = JSONRenderer().render(serializer.data)
            return HttpResponse(json, content_type="application/json")
        if int(request.GET['flag']) == QueryType.SEARCH_BY_PRODUCT_OR_CODE:
            product_detail = str(request.GET['product_detail'])
            product_queryset = product_queryset.filter(Q(item_name=product_detail)|Q(item_code=product_detail))
            serializer = ProductSerializer(
                product_queryset, many=True, context={'request': request})
            json = JSONRenderer().render(serializer.data)
            return HttpResponse(json, content_type="application/json")
        if int(request.GET['flag']) == QueryType.COUNT_ACTIVE_INACTIVE.value:
            response_data = {"active":product_queryset.filter(enabled=True).count(),"inactive":product_queryset.filter(enabled=False).count()}
            return HttpResponse(simplejson.dumps(response_data), content_type='application/json')
        if int(request.GET['flag']) == QueryType.AVERAGE_PRODUCT_PRICE.value:
            category_l1l2 = product_queryset.values('category_l2','category_l1').annotate(Avg('mrp_price'))
            json_list = list()
            for temp_dict in category_l1l2:
                json_list.append(dict(temp_dict))
            return HttpResponse(simplejson.dumps(json_list), content_type='application/json')



