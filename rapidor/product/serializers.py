from rest_framework import serializers
from .models import Product

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id','item_code','item_name','category_l1','category_l2','child_code','parent_code_path','mrp_price','size','enabled','date_created','date_updated')

