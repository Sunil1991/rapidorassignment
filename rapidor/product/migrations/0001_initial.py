# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ProductDesc',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('item_code', models.CharField(max_length=512, null=True, blank=True)),
                ('item_name', models.CharField(max_length=512, null=True, blank=True)),
                ('category_l1', models.CharField(max_length=512, null=True, blank=True)),
                ('category_l2', models.CharField(max_length=512, null=True, blank=True)),
                ('child_code', models.CharField(max_length=512, null=True, blank=True)),
                ('parent_code_path', models.CharField(max_length=512, null=True, blank=True)),
                ('mrp_price', models.IntegerField(default=0, null=True, blank=True)),
                ('size', models.IntegerField(default=0, null=True, blank=True)),
                ('enabled', models.BooleanField(default=False)),
                ('date_created', models.DateField(null=True, blank=True)),
                ('date_updated', models.DateField(null=True, blank=True)),
            ],
        ),
    ]
