from enum import IntEnum


class QueryType(IntEnum):
    LOAD = 1
    COUNT_ACTIVE_INACTIVE = 2
    SEARCH_BY_PRODUCT_OR_CODE = 3
    SEARCH_BY_PRODUCT = 4
    AVERAGE_PRODUCT_PRICE = 5