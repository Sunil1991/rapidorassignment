from django.db import models

class Product(models.Model):
    id = models.AutoField(primary_key=True)
    item_code = models.CharField(max_length=512, null=True, blank=True)
    item_name = models.CharField(max_length=512, null=True, blank=True)
    category_l1 = models.CharField(max_length=512, null=True, blank=True)
    category_l2 = models.CharField(max_length=512, null=True, blank=True)
    child_code = models.CharField(max_length=512, null=True, blank=True)
    parent_code_path = models.CharField(max_length=512, null=True, blank=True)
    mrp_price = models.IntegerField(default=0, null=True, blank=True)
    size = models.CharField(max_length=100, null=True, blank=True)
    enabled = models.BooleanField(default=False)
    date_created = models.DateField(null=True, blank=True)
    date_updated = models.DateField(null=True, blank=True)

    def __unicode__(self):
        return str(self.id) + ':::' + str(self.item_code) + ':::' + str(self.item_name) + ':::' + str(self.customer_id) + ':::' + str(self.child_code) + ':::' + str(self.parent_code_path) + ':::' + str(self.mrp_price) +  ':::' + str(self.size) + ':::' + str(self.enabled)
