from django.conf.urls import include, url
from rest_framework import routers
from views import ProductViewset

router = routers.DefaultRouter()
router.register(r'mgmt', ProductViewset)

slashless_router = routers.DefaultRouter(trailing_slash=False)
slashless_router.registry = router.registry[:]

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(slashless_router.urls)),
]