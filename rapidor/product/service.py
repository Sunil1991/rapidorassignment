from .models import Product
import pandas as pd
child_parent_dict = dict()

"""
Maintain a child_code with its parents hierarchy to top most level
"""
def get_parent_path_dict(df):
    sub_df = df[["Child Code","Parent Code"]]
    global child_parent_dict
    child_parent_dict = prepare_child_parent_dict(sub_df)
    child_parent_path_dict = dict()
    try:
        for ind in df.index:
            if df['Child Code'][ind] not in child_parent_path_dict:
                child_code = str(df['Child Code'][ind])
                parent_code = str(child_parent_dict[child_code])
                child_parent_path_dict[child_code] = get_parent_path(parent_code)
        return child_parent_path_dict
    except Exception as e:
        print("Error while finding parent path ",e)

"""
Given child node it will recursively return the path of particular hierarchy of parent and its parent seperated by ">"
"""
def get_parent_path(parent_code):
    if parent_code == 'None':
        return parent_code
    else:
        return parent_code +">"+ get_parent_path(child_parent_dict[parent_code])

"""
Maintain a dictionary with key representing child_code and value represents parent_code
"""
def prepare_child_parent_dict(sub_df):
    child_parent_dict = dict()
    try:
        for ind in sub_df.index:
            if not pd.isnull(sub_df['Parent Code'][ind]):
                child_parent_dict[str(sub_df['Child Code'][ind])] = str(sub_df['Parent Code'][ind])
            else:
                child_parent_dict[str(sub_df['Child Code'][ind])] = 'None'
        return child_parent_dict
    except Exception as e:
        print("Error in creating sub_df ",e)

""""
Inserting excel record one by one into table by updating its corresponding parents path  
"""
def inset_dataframe_db(df,child_parent_dict):
    for ind in df.index:
        enabled = False
        if df['Enabled'][ind] == 'Yes':
            enabled = True
        try:
            Product.objects.create(item_code = df['Item Code'][ind],item_name = df['Item Name'][ind],
                               category_l1 = df['Category L1'][ind],category_l2 = df['Category L2'][ind],
                               child_code = df['Child Code'][ind],
                               parent_code_path = child_parent_dict[df['Child Code'][ind]],
                               mrp_price = df['MRP Price'][ind],size = df['Size'][ind],enabled= enabled)
        except Exception as e:
            print("Error while saving poduct details ",e)