"""
Utility function for reading general excel files and returing 2d matrix using pandas
"""

import pandas as pd
import os

def read_excel(file_name):
    BASE_DIR = os.path.dirname(os.path.dirname(__file__))
    path = os.path.join(BASE_DIR+"/excelupload/"+file_name)
    try:
        df = pd.read_excel(path, sheet_name='source')
        return df
    except Exception as e:
        print("Error in reading excel file",e)
